# vim-docker

## What is this?

This is a proof of concept of wrapping up a custom vim config into a docker image

## How to use

Add to your .bashrc or .zshrc:

```
function dvim {
        if [[ $# -eq 1 && $1 =~ ^\.\. ]]; then
                # handle ../../path when single arg
                docker run -it --rm -e PAGER -v "$(cd "$(dirname "$1")"; pwd):/app" registry.gitlab.com/mattsgarrison/vim-docker $(basename "$1")
        elif [[ $# -eq 1 && $1 =~ ^\/ ]]; then
                # handle ~/path -or- actual absolute paths when single arg
                docker run -it --rm -e PAGER -v "$(dirname "$1"):/app" registry.gitlab.com/mattsgarrison/vim-docker $(basename "$1")
        else
                # handle most everything else
                docker run -it --rm -e PAGER -v "$(pwd):/app" registry.gitlab.com/mattsgarrison/vim-docker $@
        fi
}
```

## Building Locally

```
docker build -t registry.gitlab.com/mattsgarrison/vim-docker .
```

Refreshing vim-coc plugins.
(this is a bit recursive, and there are other ways of moving the assets into the folder, like on the host system directly)

```
docker run -it --rm -v $PWD/vimplug/.config:/home/vimuser/.config registry.gitlab.com/mattsgarrison/vim-docker  .
```
